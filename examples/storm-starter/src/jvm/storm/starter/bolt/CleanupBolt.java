package storm.starter.bolt;

import java.util.Map;

import twitter4j.Status;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CleanupBolt extends BaseRichBolt {
	private OutputCollector _outputCollector;

	@Override
	public final void prepare(final Map map,
			final TopologyContext topologyContext,
			final OutputCollector outputCollector) {
		// TODO Auto-generated method stub
		this._outputCollector = outputCollector;

	}

	@Override
	public void execute(Tuple input) {

		final Status status = (Status) input.getValueByField("tweet");
		if ((status.getUser().getLang().equalsIgnoreCase("en"))) {

			if (status.getSource().contains("spotify")) {
				this._outputCollector.emit(new Values("generic", status));
			}

		}

	}

	@Override
	public final void declareOutputFields(
			final OutputFieldsDeclarer outputFieldsDeclarer) {
		// Emit the state and also the complete tweet to the next Bolt.
		outputFieldsDeclarer.declare(new Fields("generic", "tweet3"));

	}

}
