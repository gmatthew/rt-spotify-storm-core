package storm.starter.bolt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;

import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.URLEntity;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class ExtractorBolt extends BaseRichBolt {
	private OutputCollector _outputCollector;

	private String song;
	private String artist;
	private String httpUrl;
	private String twitterURL;

	private String thumbnail_url;
	private String songTitle;
	private String iframe;
	private String genre;

	private static final Pattern TAG_PATTERN = Pattern
			.compile("(?:^|\\s|[\\p{Punct}&&[^/]])(#[\\p{L}0-9-_]+)");
	//private static final String WS_ENDPOINT = "http://spotify.gerard.co/spotify-data-from-url.php?url=";
	private static final String WS_ENDPOINT = "http://delighted-reptile-0986.vagrantshare.com/spotify-data-from-url.php?url=";

	@Override
	public final void prepare(final Map map,
			final TopologyContext topologyContext,
			final OutputCollector outputCollector) {
		// TODO Auto-generated method stub
		this._outputCollector = outputCollector;

	}

	@Override
	public void execute(Tuple input) {
		try {
			// final String state = (String) input.getValueByField("generic");
			final Status status = (Status) input.getValueByField("tweet3");

			String pattern = "\\\"?([\\w\\d\\s-'#\\(\\))]*)\\\"?\\s(by|de)\\s([\\w|\\s|\\.]*)\\sfrom?";
			String pattern2 = "\\\"?([\\w\\d\\s-'#\\(\\))]*)\\\"?\\s(by|de)\\s([\\w|\\s|\\.]*)\\s?";
			Matcher pattern5 = TAG_PATTERN.matcher(status.getText());
			String cleanedText = pattern5.replaceAll("");
			Pattern pattern1 = Pattern.compile(pattern);
			Matcher matcher = pattern1.matcher(cleanedText);

			Long timestamp1 = new Date().getTime();

			URLEntity[] urlEntities = status.getURLEntities();

			// Based on the metadata this should run only once
			if (urlEntities != null) {
				for (int i = 0; i < urlEntities.length; i++) {
					URLEntity urlEntity = urlEntities[i];
					httpUrl = urlEntity.getExpandedURL().replace("\"", "");

				}

			}

			twitterURL = "https://twitter.com/statuses/" + status.getId();
			if (matcher.find()) {

				song = matcher.group(1).replace("\"", "").trim();
				artist = matcher.group(3).replace("\"", "").trim();

			} else {
				Pattern pattern3 = Pattern.compile(pattern2);
				Matcher matcher2 = pattern3.matcher(cleanedText);
				if (matcher2.find()) {
					song = matcher2.group(1).replace("\"", "").trim();
					artist = matcher2.group(3).replace("\"", "").trim();

				}

			}

			// webservice call
			String wsResp = getJSON(WS_ENDPOINT + httpUrl, 0);
			System.out.println(" wsResp +++++++++++++++++++++++++++++ "
					+ wsResp);
			if (wsResp != null) {
				StringReader wsRespReader = new StringReader(wsResp);
				JsonReader jsonReader = Json.createReader(wsRespReader);
				JsonObject jsonObject = jsonReader.readObject();
				thumbnail_url = jsonObject.get("thumbnail_url").toString()
						.replace("\"", "");
				songTitle = jsonObject.get("title").toString()
						.replace("\"", "");
				iframe = jsonObject.get("html").toString().replace("380", "80")
						.replace("\"", "").replace("\\", "").replace("\"<", "")
						.replace(">\"", "");
				genre = jsonObject.get("genre").toString().replace("\"", "");
				jsonReader.close();
			}

			// Build Message

			// http://docs.oracle.com/javaee/7/api/javax/json/JsonObjectBuilder.html
			JsonObjectBuilder songBuilder = Json.createObjectBuilder();

			JsonObject value = null;
			if ((song != null && !song.isEmpty())
					&& (artist != null && !artist.isEmpty())
					&& (httpUrl != null && !httpUrl.isEmpty())) {
				value = songBuilder
						.add("song", song)
						.add("artist", artist)
						.add("link", httpUrl)
						// .add("original", status.toString())
						.add("tweets",
								Json.createArrayBuilder().add(
										Json.createObjectBuilder()
												.add("url", twitterURL)
												.add("timestamp", timestamp1)))
						.add("thumbnail", thumbnail_url)
						.add("song_title", songTitle).add("iframe", iframe)
						.add("genre", genre).add("count", 1).build();
			}
			if (value != null)
				this._outputCollector.emit(new Values(value.toString(),
						"finalmessage"));
		} catch (Exception ex) {

			System.out
					.println(" ***********Exception in ExtractorBolt ****************");
			ex.printStackTrace();
		} finally {
			song = null;
			artist = null;
			httpUrl = null;
		}

	}

	public final void declareOutputFields(
			final OutputFieldsDeclarer outputFieldsDeclarer) {
		// Emit the state and also the complete tweet to the next Bolt.
		outputFieldsDeclarer.declare(new Fields("finalmessage", "tweet4"));
	}

	public String getJSON(String url, int timeout) {
		HttpURLConnection c = null;
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				return sb.toString();
			}

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (c != null) {
				try {
					c.disconnect();
				} catch (Exception ex) {
				}
			}
		}
		return null;
	}

}
