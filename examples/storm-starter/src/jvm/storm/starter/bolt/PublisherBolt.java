package storm.starter.bolt;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.storm.json.simple.JSONObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import twitter4j.Status;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

public class PublisherBolt extends BaseRichBolt {

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {

		final String finalmessage = (String) input
				.getValueByField("finalmessage");
		// Redis connectivity
		JedisPool pool = new JedisPool(new JedisPoolConfig(),
				"spotify.gerard.co");
		// / Jedis implements Closable. Hence, the jedis instance will be
		// auto-closed after the last statement.
		try (Jedis jedis = pool.getResource()) {
			if (finalmessage != null) {
				System.out.println("[PublisherBolt][log:finalmessage"
						+ finalmessage);
				jedis.publish("twitter", finalmessage);

			}
		} catch (Exception ex) {

			System.out.println("something wrong with Redis connection");
			ex.printStackTrace();
		} finally {
			if (pool != null)
				pool.destroy();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub

	}

}
