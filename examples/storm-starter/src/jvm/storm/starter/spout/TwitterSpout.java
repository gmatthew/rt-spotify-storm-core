package storm.starter.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public final class TwitterSpout extends BaseRichSpout {
	/**
	 * 
	 */
	private SpoutOutputCollector _outputCollector;
	private LinkedBlockingQueue<Status> _queue;
	private TwitterStream _twitterStream;

	String consumerKey;
	String consumerSecret;
	String accessToken;
	String accessTokenSecret;
	String[] keyWords;

	public TwitterSpout(String consumerKey, String consumerSecret,
			String accessToken, String accessTokenSecret, String[] keyWords) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.accessToken = accessToken;
		this.accessTokenSecret = accessTokenSecret;
		this.keyWords = keyWords;
	}

	@Override
	public final void open(final Map conf, final TopologyContext context,
			final SpoutOutputCollector collector) {
		this._queue = new LinkedBlockingQueue<>(1000);
		this._outputCollector = collector;

		final StatusListener statusListener = new StatusListener() {
			@Override
			public void onStatus(final Status status) {
				_queue.offer(status);
			}

			@Override
			public void onDeletionNotice(final StatusDeletionNotice sdn) {
			}

			@Override
			public void onTrackLimitationNotice(final int i) {
			}

			@Override
			public void onScrubGeo(final long l, final long l1) {
			}

			@Override
			public void onStallWarning(final StallWarning stallWarning) {
			}

			@Override
			public void onException(final Exception e) {
			}
		};

		final ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setIncludeEntitiesEnabled(true);

		configurationBuilder.setOAuthAccessToken(accessToken);
		configurationBuilder.setOAuthAccessTokenSecret(accessTokenSecret);
		configurationBuilder.setOAuthConsumerKey(consumerKey);
		configurationBuilder.setOAuthConsumerSecret(consumerSecret);

		this._twitterStream = new TwitterStreamFactory(configurationBuilder
				.setJSONStoreEnabled(true).build()).getInstance();
		this._twitterStream.addListener(statusListener);

		// Our usecase computes the sentiments of States of USA based on tweets.
		// So we will apply filter with US bounding box so that we get tweets
		// specific to US only.
		final FilterQuery filterQuery = new FilterQuery();

		// Bounding Box for United States.
		// For more info on how to get these coordinates, check:
		// http://www.quora.com/Geography/What-is-the-longitude-and-latitude-of-a-bounding-box-around-the-continental-United-States
		// final double[][] boundingBoxOfUS = { { -124.848974, 24.396308 },
		// { -66.885444, 49.384358 } };
		// filterQuery.locations(boundingBoxOfUS);
		// FilterQuery query = new FilterQuery().track(keyWords);
		// twitterStream.filter(query);
		final double[][] boundingBoxOfUS = { { -124.848974, 24.396308 },
				{ -66.885444, 49.384358 } };
		final double[] sfo = {-122.75,36.8,-121.75,37.8};
		filterQuery.locations(boundingBoxOfUS);
		filterQuery.track(keyWords);
		this._twitterStream.filter(filterQuery);
	}

	@Override
	public final void nextTuple() {
		final Status status = _queue.poll();
		// System.out.println("******** TwitterSpout *********" + status);
		if (null == status) {
			// If _queue is empty sleep the spout thread so it doesn't consume
			// resources.
			Utils.sleep(500);
		} else {
			// Emit the complete tweet to the Bolt.
			this._outputCollector.emit(new Values(status));
		}
	}

	@Override
	public final void close() {
		this._twitterStream.cleanUp();
		this._twitterStream.shutdown();
	}

	@Override
	public final void ack(final Object id) {
	}

	@Override
	public final void fail(final Object id) {
	}

	@Override
	public final void declareOutputFields(
			final OutputFieldsDeclarer outputFieldsDeclarer) {
		// For emitting the complete tweet to the Bolt.
		outputFieldsDeclarer.declare(new Fields("tweet"));
	}
}