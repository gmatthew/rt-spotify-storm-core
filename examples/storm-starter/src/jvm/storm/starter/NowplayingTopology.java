package storm.starter;

import storm.starter.bolt.CleanupBolt;
import storm.starter.bolt.ExtractorBolt;
import storm.starter.bolt.PublisherBolt;
import storm.starter.spout.TwitterSpout;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;

public class NowplayingTopology {
	private static final String consumerKey = "MmhR6DPfn3KdCjdjzn5g1zwPS";
	private static final String consumerSecret = "QPjvOzL9KL8LtpqLLKDg2kcz10HFIuu9LZCEIOnVp3NiQibLEc";
	private static final String accessToken = "68490583-vUyZMto5YfNeVCraFWORNDNwCmL0SD5Tav1iZGdH3";
	private static final String accessTokenSecret = "oOLfFb2lKSezz0pBGK3YBY73RKyTzxgAzAXHC59uhtEAg";

	public static final void main(final String[] args) throws Exception {
		final LocalCluster localCluster = new LocalCluster();
		try {
			final Config config = new Config();
			config.setMessageTimeoutSecs(120);
			config.setDebug(false);

			String spoutId = "twitterspout";
			String cleanupId = "cleanup";
			String extractorId = "extractor";
			String publisherId = "publisher";

			String[] keyWords = { "#nowplaying", "#listenlive", "#np",
					"spotify" };

			final TopologyBuilder topologyBuilder = new TopologyBuilder();

			topologyBuilder.setSpout(spoutId, new TwitterSpout(consumerKey,
					consumerSecret, accessToken, accessTokenSecret, keyWords));

			topologyBuilder.setBolt(cleanupId, new CleanupBolt())
					.shuffleGrouping(spoutId);
			topologyBuilder.setBolt(extractorId, new ExtractorBolt())
					.fieldsGrouping(cleanupId, new Fields("generic"));
			topologyBuilder.setBolt(publisherId, new PublisherBolt())
					.fieldsGrouping(extractorId, new Fields("finalmessage"));

			// Submit it to the cluster, or submit it locally
			if (null != args && 0 < args.length) {
				config.setNumWorkers(3);
				StormSubmitter.submitTopology(args[0], config,
						topologyBuilder.createTopology());
			} else {
				config.setMaxTaskParallelism(10);

				localCluster.submitTopology("nowplaying", config,
						topologyBuilder.createTopology());
				// Run this topology for 120 seconds so that we can complete
				// processing of decent # of tweets.
				while (true)
					Utils.sleep(120 * 1000);

			}

		} catch (final AlreadyAliveException | InvalidTopologyException exception) {
			// Deliberate no op;
			exception.printStackTrace();
		} catch (final Exception exception) {

			if (localCluster != null) {
				localCluster.killTopology("nowplaying");
				localCluster.shutdown();

				Runtime.getRuntime().addShutdownHook(new Thread() {

					@Override
					public void run() {
						localCluster.killTopology("nowplaying");
						localCluster.shutdown();
					}
				});
			}

			// Deliberate no op;
			exception.printStackTrace();
		}

	}
}
